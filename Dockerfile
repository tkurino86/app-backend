FROM php:7.3-fpm-alpine

RUN apk --no-cache update && apk --no-cache upgrade \
    && docker-php-ext-install \
    bcmath \
    pdo_mysql

# multi-stage builds
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer 
RUN composer global require hirak/prestissimo


